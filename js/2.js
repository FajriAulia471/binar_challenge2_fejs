function checkTypeNumber(givenNumber) {
  if (givenNumber === undefined) {
    //untuk mengecek jika parameter kosong
    return "Error: Bro where is the parameter?";
  } else if (typeof givenNumber != "number") {
    //untuk mengecek sesuai dengan tipe data number
    return "Error: Invalid data type";
  } else if (givenNumber % 2 == 0) {
    //modulus operator: mengeluarkan sisa pembagian
    return "GENAP";
  } else {
    return "GANJIL";
  }
}

console.log(checkTypeNumber(3));
console.log(checkTypeNumber(10));
console.log(checkTypeNumber("3"));
console.log(checkTypeNumber({}));
console.log(checkTypeNumber([]));
console.log(checkTypeNumber());
