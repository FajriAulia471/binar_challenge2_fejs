function checkEmail(email) {
  var regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

  if (email === undefined) {
    //mengecek jika tidak memiliki parameter
    return "ERROR : email has no parameters";
  } else if (typeof email == "number") {
    //mengecek jika hanya berisi angka
    return "ERROR : email cannot be just numbers";
  } else if (/@/.test(email) == false) {
    // method test() : mengeksekusi pencarian kecocokan antara regular expression dengan streing tertentu
    return "Error not complete";
  } else if (regex.test(email)) {
    return "VALID";
  } else {
    return "INVALID";
  }
}

console.log(checkEmail("apranata@binar.co.id"));
console.log(checkEmail("apranata@binar.com"));
console.log(checkEmail("apranata@binar"));
console.log(checkEmail("apranata"));
console.log(checkEmail(3322));
console.log(checkEmail());
