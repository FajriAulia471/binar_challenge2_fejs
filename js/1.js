function changeWord(selectedText, changedText, text) {
  //keyoword var = ini merupakan function scope, agar diluar function tidak bisa mengakses variable tersebut
  //line dibawah ini berfungsi untuk mengambil text yang akan diubah
  var kalimat = text.replace(selectedText, changedText);
  return kalimat;
  //return berfungsi untuk mengembalikan nilai
}

const kalimat1 = "Andini sangat mencintai kamu selamanya";
const kalimat2 =
  "Gunung Bromo tak akan mampu menggambarkan besarnya cintaku padamu";

console.log(changeWord("mencintai", "membenci", kalimat1));
console.log(changeWord("Bromo", "Semeru", kalimat2));
