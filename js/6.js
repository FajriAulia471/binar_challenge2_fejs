function getAngkaTerbesarKedua(dataNumbers) {
  if (dataNumbers === undefined)
    return "Error : function need as array parameter";
  if (!Array.isArray(dataNumbers)) return "Error : Invalid data type"; //Metode statis Array.isArray() menentukan apakah nilai yang dicek adalah sebuah Array.
  if (dataNumbers.length < 2)
    return "Error : Array length should be 2 or longer";

  let first = Number.MIN_SAFE_INTEGER; //Properti data statis Number.MIN_SAFE_INTEGER mewakili bilangan bulat aman minimum dalam JavaScript
  let second = Number.MIN_SAFE_INTEGER;
  let isContainMinVal = false;

  dataNumbers.forEach((number) => {
    if (number > first) {
      first = number;
    } else if (number !== first && number > second) {
      second = number;
    } else if (number === Number.MIN_SAFE_INTEGER) {
      isContainMinVal = true;
    }
  });

  if (second !== Number.MIN_SAFE_INTEGER || isContainMinVal) return second; //or
  else return "there is no second largest number";
}

const dataNumbers = [9, 4, 7, 7, 4, 3, 2, 2, 8];
console.log(getAngkaTerbesarKedua(dataNumbers));
console.log(getAngkaTerbesarKedua(0));
console.log(getAngkaTerbesarKedua());
