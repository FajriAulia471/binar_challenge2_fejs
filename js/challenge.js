// 1
// function changeWord(selectedText, changedText, text) {
//   return text.replace(selectedText, changedText);
// }

// const kalimat1 = "Andini sangat mencintai kamu selamanya";
// const kalimat2 =
//   "Gunung Bromo tak akan mampu menggambarkan besarnya cintaku padamu";

// console.log(changeWord("mencintai", "membenci", kalimat1));
// console.log(changeWord("Bromo", "Semeru", kalimat2));

// 2
// function checkTypeNumber(givenNumber) {
//   if (typeof givenNumber != "number") {
//     return "UNDEFINED";
//   } else if (givenNumber % 2 === 0) {
//     return "GENAP";
//   } else {
//     return "GANJIL";
//   }
// }

// console.log(checkTypeNumber(3));
// console.log(checkTypeNumber(10));
// console.log(checkTypeNumber("3"));
// console.log(checkTypeNumber({}));
// console.log(checkTypeNumber([]));
// console.log(checkTypeNumber());

// 3
// function checkEmail(email) {
//   const emailRegex = new RegExp(
//     /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
//   );
//   const validEmail = emailRegex.test(email);
//   const emailRegex2 = new RegExp(/[A-Z0-9]/gi);
//   const errorEmail = emailRegex2.test(email);
//   const emailRegex3 = new RegExp(/@/);
//   const addEmail = emailRegex3.test(email);

//   let validation;
//   if (email == null) {
//     //undefined
//     validation = "Error parameter kosong";
//   } else if (
//     errorEmail == true &&
//     addEmail == false &&
//     typeof email == "string"
//   ) {
//     validation = "Error not complete";
//   } else if (validEmail == true) {
//     validation = "Valid";
//   } else if (validEmail == false) {
//     validation = "Invalid";
//   }

//   return validation;
// }

// console.log(checkEmail("apranata"));

// 4
// function isValidPassword(email) {
//   if (typeof email == "undifined") {
//     return "Error anda belum memasukan password";
//   } else if (email.length < 8) {
//     return "Password kurang dari 8";
//   } else if (!/[A-Z]/.test(email)) {
//     return "Password tidak memiliki huruf kapital";
//   } else if (!/[a-z]/.test(email)) {
//     return "Password tidak memiliki huruf kecil";
//   } else if (!/[0-9]/.test(email)) {
//     return "Password tidak memiliki angka";
//   } else if (typeof email != "string") {
//     return "Password bukan string!";
//   } else {
//     return "Password sesuai";
//   }
// }

// console.log(isValidPassword("Fajri12345"));

//5
// function getSplitName(personName) {
//   let result = personName.split(" ");
//   if (result.length == 3) {
//     return `firstName = ${result[0]},  middleName = ${result[1]}, lastName = ${result[2]}`;
//   } else if (result.length == 2) {
//     return `firstName = ${result[0]},  middleName = null, lastName = ${result[1]}`;
//   } else if (result.length == 1) {
//     return `firstName = ${result[0]},  middleName = null, lastName = null`;
//   } else {
//     return "This function is only for 3 characters name";
//   }
// }

// console.log(getSplitName("Aldi Danella Pranata"));
// console.log(getSplitName("Dwi Kuncoro"));
// console.log(getSplitName("Aurora"));
// console.log(getSplitName("Aurora Aurellia Sukma Dharma"));
// // console.log(getSplitName(0));
