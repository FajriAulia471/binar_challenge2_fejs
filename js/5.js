function getSplitName(personName) {
  if (typeof personName !== "string") return "Error: Invalid Type.";

  var result = personName.split(" ");
  /* method split () : mengambil sebuah pola dan membagi string ke dalam sebuah substring 
  yang diurutkan dengan mencari polanya, menempatkan kedalam sebuah array dan mengembalikannya */

  if (result.length == 3) {
    return `firstName = ${result[0]},  middleName = ${result[1]}, lastName = ${result[2]}`;
  } else if (result.length == 2) {
    return `firstName = ${result[0]},  middleName = null, lastName = ${result[1]}`;
  } else if (result.length == 1) {
    return `firstName = ${result[0]},  middleName = null, lastName = null`;
  } else {
    return "This function is only for 3 characters name";
  }
}

console.log(getSplitName("Aldi Danella Pranata"));
console.log(getSplitName("Dwi Kuncoro"));
console.log(getSplitName("Aurora"));
console.log(getSplitName("Aurora Aurellia Sukma Dharma"));
console.log(getSplitName(0));
