const dataPenjualanPakAldi = [
  {
    namaProduct: "Sepatu Futsal Nike Vapor Academy B",
    hargaSatuan: 760000,
    kategori: "Sepatu Sport",
    totalTerjual: 90,
  },
  {
    namaProduct: "Sepatu Warrior Tristan Black Brown High - Original",
    hargaSatuan: 960000,
    kategori: "Sepatu Sneaker",
    totalTerjual: 37,
  },
  {
    namaProduct: "Sepatu Warrior Tristan Maroon High - Original",
    kategori: "Sepatu Sneaker",
    hargaSatuan: 360000,
    totalTerjual: 90,
  },
  {
    namaProduct: "Sepatu Warrior Rainbow Tosca Conduroy - [BNIB] Original",
    hargaSatuan: 120000,
    kategori: "Sepatu Sneaker",
    totalTerjual: 90,
  },
];

// function hitungTotalPenjualan(dataPenjualan) {
//   if (!Array.isArray(dataPenjualan)) return "Error : Data must be an array";

//   const totalPenjualan = dataPenjualan.reduce((total, data) => {
//     //reduce akan melakukan penjumlahan dari element pertama array kemudian ke element seterusnya
//     return total + data.totalTerjual;
//   }, 0);

//   if (isNaN(totalPenjualan)) {
//     // isNaN() menentukan apakah sebuah nilai adalah NaN ketika dikonversi ke angka
//     return "Error : format data salah";
//   } else {
//     return totalPenjualan;
//   }
// }

function hitungTotalPenjualan(dataPenjualan) {
  let terjual =
    dataPenjualan[0].totalTerjual +
    dataPenjualan[1].totalTerjual +
    dataPenjualan[2].totalTerjual +
    dataPenjualan[3].totalTerjual;

  return terjual;
}

console.log(hitungTotalPenjualan(dataPenjualanPakAldi));
