function isValidPassword(email) {
  if (email === undefined) {
    return "Error : you have not entered a password";
  } else if (typeof email === "number") {
    return "ERROR : passwords require letters";
  } else if (email.length < 8) {
    return "Password less than 8 letters";
  } else if (!/[A-Z]/.test(email)) {
    return "Password does not have a capital letter";
  } else if (!/[a-z]/.test(email)) {
    return "Password has no lowercase letters";
  } else {
    return "Password match ";
  }
}

console.log(isValidPassword("Meong2021"));
console.log(isValidPassword("meong2021"));
console.log(isValidPassword("@eong"));
console.log(isValidPassword("Meong2"));
console.log(isValidPassword(0));
console.log(isValidPassword());
